DBA Weekly Checklist



1. Check for objects that don't fit standards

2. Check for database and server configuration policy violations

3. Check for security policy violations

4. Check for new versions of software and updates

5. Review data growth rates per customer
